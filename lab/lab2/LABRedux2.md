we’ve put all redux logic into a single redux-mixin.js file.

This can get quite messy because:

1.Reducers are most likely to get blown up.
2.In our Polymer elements we hard code the action types, if something changes, we need to change it everywhere.
3.Don’t repeat yourself. If our requirements for an object’s normalisation changes, we want to execute that change in a single place.

So let’s create 3 more files:

redux-events.js
redux-reducer.js
redux-selector.js

install reselect

```
npm install --save reselect
```

Reselect let us do a reselection of the property or combine certain props think of array property 1 array property 2 index

Let's start with the redux-selector file

you can import reselector by keyword so 

```
import { createSelector } from 'reselect'
```

createSelector works as follows
createSelector(...inputSelectors | [inputSelectors], resultFunc)

so we create a function that gets the todos

```
const getTodos = (state) => {
    return state.todos;
};
```


and we export the to uppercase property

```
export const todosUpper = createSelector([getTodos],(todos)=>{//implement}

```
note that todos is an array of objects;
{text:x}


now change the property in list to 
```
static mapStateToProps(state, element) {
        return {
            todos: todosUpper(state)
        };
    }
```
If you need to change the delete methode in the reducer do that now

take the redux-event file

we will create to function here wo will return a function

```
export const removeTodo = //todo implement
```
so we can change this in todo-list
```
static mapDispatchToEvents(dispatch, element) {
	return {
		removeTodo: removeTodo(dispatch)
	};
}
```
now we will move the reducer to his own file and export it to the mixin

```
import {reducer} from "./redux-reducers";
````




