import {createStore} from '../../node_modules/redux/src/index.js'
import {createMixin} from '../../node_modules/polymer-redux/polymer-redux.js'
import {reducer} from "./redux-reducers";

// Create a Redux store
const store = createStore(reducer, /* preloadedState, */window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
export const ReduxMixin = createMixin(store);