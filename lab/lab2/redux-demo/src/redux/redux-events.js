const returndispatchfunction = function (dispatch){
    const dispatchFunction = function (event) {
        return dispatch({
            type: 'REMOVE_TODO',
            todo: event.detail
        });
    };
    return dispatchFunction
};

export const removeTodo = returndispatchfunction;




export const addTodo = (dispatch)=>{
    return (event)=>{
        dispatch({
            type: 'ADD_TODO',
            todo: event.detail
        })
    }
};