const initialState = {
    todos: [
        {text: 'Buy Veggies'},
        {text: 'Buy Fruits'},
        {text: 'Buy Pizza'},
    ],
};

export const reducer = (state, action) => {
    if (!state) {
        return initialState;
    }
    const todos = state.todos.slice(0); //copy
    switch (action.type) {
        case 'ADD_TODO':
            todos.push({text:action.todo});
            break;
        case 'REMOVE_TODO':
            todos.splice(findIndexOfTodo(todos,'text',action.todo),1);
            break;
    }
    return Object.assign({}, state, {todos: todos});
};

function findIndexOfTodo(array, attr, value) {
    for(let i = 0; i < array.length; i += 1) {
        if(array[i][attr].toLowerCase() === value.toLowerCase()) {
            return i;
        }
    }
    return -1;
}