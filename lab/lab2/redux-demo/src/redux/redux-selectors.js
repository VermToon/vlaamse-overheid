import {createSelector} from 'reselect'

const getTodos = (state) => {
    return state.todos;
};

export const todos = createSelector([getTodos],
    (todos) => {
        return todos;
    }
);


export const todosUpper = createSelector([getTodos],
    (todos) => {
        return todos.map(t => {text:t.text.toUpperCase()})
    }
);