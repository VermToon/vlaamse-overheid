import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import {ReduxMixin} from '../redux/redux-mixin.js'
import {todosUpper} from '../redux/redux-selectors.js'
import {removeTodo} from "../redux/redux-events";

/**
 * @customElement
 * @polymer
 */
export class Todo extends ReduxMixin(PolymerElement) {
    static get is() {
        return 'todo-list';
    }

    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
        </style>
        <ul>
            <dom-repeat items="[[todos]]">
                <template>
                    <li>
                        <button on-click="_remove">[[item.text]]</button>
                    </li>               
                </template>
            </dom-repeat>
        </ul>
    `;
    }

    static get properties() {
        return {
            todos: {
                type: Array,
                readOnly: true,
            },
        };
    }

    static mapStateToProps(state, element) {
        return {
            todos: todosUpper(state)
        };
    }

    static mapDispatchToEvents(dispatch, element) {
        return {
            removeTodo: removeTodo(dispatch)
        };
    }

    _remove(e) {
        const todo = e.target.innerHTML;
        this.dispatchEvent(
            new CustomEvent('remove-todo', {
                detail: todo
            })
        );
    }
}

window.customElements.define(Todo.is, Todo);