import {createStore,applyMiddleware} from '../../node_modules/redux/src/index.js'
import {createMixin} from '../../node_modules/polymer-redux/polymer-redux.js'
import {reducer} from "./redux-reducers";
import thunk from 'redux-thunk';


// Create a Redux store
const store = createStore(
    reducer,
    // applyMiddleware(thunk)
    );


export const ReduxMixin = createMixin(store);


