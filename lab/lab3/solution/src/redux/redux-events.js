export const removeTodo = (dispatch) => {
    return (event) => {
        dispatch({
            type: 'REMOVE_TODO',
            todo: event.detail.value
        })
    }
};

export const addTodo = (dispatch) => {
    return (event) => {
        dispatch({
            type: 'ADD_TODO',
            todo: event.detail
        })
    }
};
