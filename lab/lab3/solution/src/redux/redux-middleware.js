import {addTodo, removeTodo} from "./redux-events";

const url = 'http://localhost:3000/todos';

export const fetchData = (dispatch) => {
    return fetch(url) // Redux Thunk handles these
        .then(res => res.json())
        .then(
            data => dispatch({type: 'LOAD_DATA_SUCCESS', data}),
            err => dispatch({type: 'ERROR', err})
        );
};






export const postAddTodo = (dispatch) => {
    return (event) => {
        const todo = {text: event.detail};
        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'post',
            body: JSON.stringify(todo)
        })
            .then(res => res.json())
            .then(
                data => addTodo(dispatch)(event),
                err => dispatch({type: 'ERROR', err})
            );
    }
};


export const deleteRemoveTodo = (dispatch) => {
    return (event) => {
        const id = Number(event.detail.index) + 1;
        console.log(event.detail.index);
        fetch(url + "/" + id, {
            method: 'Delete',
        })
            .then(res => res.json())
            .then(
                data => removeTodo(dispatch)(event),
                err => dispatch({type: 'ERROR', err})
            );
    }
};


