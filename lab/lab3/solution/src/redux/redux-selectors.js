import { createSelector } from 'reselect'

const getTodos = (state) => {
    return state.todos;
};

export const todos = createSelector([getTodos],
    (todos) => {
        return todos;
    }
);
export const todosUpper = createSelector([getTodos],
    (todos) => {
        var x=todos.map(t => t.text.toUpperCase()).map(stringToObject);
        return x;
    }
);

const stringToObject= (x)=>{return {text:x}};