import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import {ReduxMixin} from '../redux/redux-mixin.js'
import {todosUpper} from '../redux/redux-selectors.js'
import {removeTodo} from "../redux/redux-events";
import {deleteRemoveTodo, fetchData} from "../redux/redux-middleware";

/**
 * @customElement
 * @polymer
 */
export class Todo extends ReduxMixin(PolymerElement) {
    static get is() {
        return 'todo-list';
    }
    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
        </style>
        <ul>
            <dom-repeat items="[[todos]]">
                <template>
                    <li>
                        <button on-click="_remove" data-index$=[[index]]>[[item.text]]</button>
                    </li>
                </template>
            </dom-repeat>
        </ul>
    `;
    }
    constructor(){
        super();
        this.dispatchEvent(
            new CustomEvent('fetch-data')
        );
    }
    static get properties() {
        return {
            todos: {
                type: Array,
                readOnly: true,
            },
        };
    }

    static mapStateToProps(state, element) {
        return {
            todos: todosUpper(state)
        };
    }

    static mapDispatchToEvents(dispatch, element) {
        return {
            removeTodo: deleteRemoveTodo(dispatch),
            fetchData: fetchData(dispatch)
        };
    }

    _remove(e) {
        const element= e.target;
        console.log(element.getAttribute("data-index"));
        const todo = {
            index: element.getAttribute("data-index"),
            value: element.innerHTML
        };
        this.dispatchEvent(
            new CustomEvent('remove-todo', {
                detail: todo
            })
        );
    }
}
window.customElements.define(Todo.is, Todo);