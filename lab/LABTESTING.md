## Lab 1. Unit Testing 
In this lab you will create and use a web component in HTML5.
> duration: 30 minutes

### Step 1. Create an folder and use polymer init to create a polymer 3 app
```
polymer init
```
select polymer-3-application - A simple Polymer 3.0 application
choose a name and a mainclass

Run the page inside Chrome. 
```
polymer serve
```

### Step 2.  standard Unit Testing 

run the unit test, this could take a while so you can try to run it only on chrome.

```
polymer test

polymer test -l chrome
```

### Step 3. my first unit test

Create a new element that has a button.

when this button is clicked, it will change a property;

you can choose wich property and in what it will change;

Now wright a test that checks if this works.
```js
expect(element.property).to.equal('foo');
```

now change the property to a car:
```
{type:"Fiat", model:"500", color:"white"};
```

and change the model with the button;

change your test so it succeeds

```
expect(element.car).......
```

## Step 4. my first async test
now add a button that throws an event when clicked.
```
this.dispatchEvent(new CustomEvent('add', { detail: this.car, bubbles: true }));
```

create a test that will capture this event. 
- use an Asynchronous tests with done()


## step 4.1 flush and Fakeserver
npm install sinon-chai
npm install sinon


Create a fake server with sinon.

let it respond with data

create a test that validates the correct rendering of the list.



## Step 5. My e2e test configuration

git clone https://gitlab.com/VermToon/todo-project.git

Install webdriverio
```
npm install webdriverio --save-dev
```

install and run selenium-standalone
```
npm install selenium-standalone -g
selenium-standalone install && selenium-standalone start
```

run configuration from webdriverio
```
./node_modules/.bin/wdio config
```

use these values

? Where do you want to execute your tests? On my local machine
? Which framework do you want to use? cucumber
? Shall I install the framework adapter for you? Yes
? Where are your feature files located? ./features/**/*.feature
? Where are your step definitions located? ./features/step-definitions/*.js
? Which reporter do you want to use? 
? Do you want to add a service to your test setup? 
? Level of logging verbosity: silent
? In which directory should screenshots gets saved if a command fails? ./errorShots/
? What is the base url? http://localhost:port


use this command to start your e2e test
```
./node_modules/.bin/wdio wdio.conf.js
```

## Step 6. My e2e test

wright a test that adds an Todo and see if this is added.


