import {createStore} from '../../node_modules/redux/src/index.js'
import {createMixin} from '../../node_modules/polymer-redux/polymer-redux.js'

const initialState = {
    todos: [
        {text: 'Buy Veggies'},
        {text: 'Buy Fruits'},
        {text: 'Buy Pizza'},
    ],
};
const reducer = (state, action) => {
    if (!state) {
        return initialState;
    }
    const todos = state.todos.slice(0); //copy
    switch (action.type) {
        case 'ADD_TODO':
            todos.push({text:action.todo});
            break;
        case 'REMOVE_TODO':
            todos.splice(findIndexOfTodo(todos,'text',action.todo),1);
            break;
    }
    return Object.assign({}, state, {todos: todos});
};

function findIndexOfTodo(array, attr, value) {
    for(let i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

// Create a Redux store
const store = createStore(reducer);
export const ReduxMixin = createMixin(store);