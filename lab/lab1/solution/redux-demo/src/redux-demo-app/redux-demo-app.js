import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {Todo} from '../elements/todo-list';
import {TodoInput} from '../elements/todo-input';


/**
 * @customElement
 * @polymer
 */
class ReduxDemoApp extends PolymerElement {
    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
        </style>
        <!--<todo-input></todo-input>-->
        <todo-list></todo-list>
    `;
    }
}

window.customElements.define('redux-demo-app', ReduxDemoApp);
