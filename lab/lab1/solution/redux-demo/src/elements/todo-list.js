import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import {DomRepeat} from '@polymer/polymer/lib/elements/dom-repeat.js';
import {ReduxMixin} from '../redux/redux-mixin.js'

/**
 * @customElement
 * @polymer
 */
export class Todo extends ReduxMixin(PolymerElement) {
    static get is() {
        return 'todo-list';
    }
    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
        </style>
        <ul>
            <dom-repeat items="[[todos]]">
                <template>
                    <li>
                        <button on-click="_remove">[[item.text]]</button>
                    </li>
                </template>
            </dom-repeat>
        </ul>
    `;
    }

    static get properties() {
        return {
            todos: {
                type: Array,
                readOnly: true,
            },
        };
    }

    static mapStateToProps(state, element) {
        return {
            todos: state.todos
        };
    }
    static mapDispatchToEvents(dispatch, element) {
        return {
            removeTodo: (event) => dispatch({
                type: 'REMOVE_TODO',
                todo: event.detail
            })
    };
    }

    _remove(e) {
        const todo = e.target.innerHTML;
        this.dispatchEvent(
            new CustomEvent('remove-todo', {
                detail: todo
            })
        );
    }
}
window.customElements.define(Todo.is, Todo);