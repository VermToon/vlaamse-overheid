import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {ReduxMixin} from '../redux/redux-mixin.js'
import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/paper-input/paper-input.js';


/**
 * @customElement
 * @polymer
 */
export class TodoInput extends ReduxMixin(PolymerElement) {
    static get is() {
        return 'todo-input';
    }

    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
            
            #input{
                min-width:400px;
            }
        </style>
        <paper-input id="input" label="Add todo (press enter)" on-keydown="_addOnEnter"></paper-input>

    `;
    }

    static mapDispatchToEvents(dispatch, element) {
        return {
            addTodo: event => dispatch({
                type: 'ADD_TODO',
                todo: event.detail
            })
        };
    }


    _addOnEnter(e) {
        const input = this.$.input;
        if (e.keyCode !== 13 || !input.value) {
            return;
        }
        // 'add' refers to the above created action
        this.dispatchEvent(
            new CustomEvent('add-todo', {
                detail: input.value
            })
        );
        input.value = null;
    }
}

window.customElements.define(TodoInput.is, TodoInput);