for we begin add this in a script tag to index.html
```
 window.process = {
          env: {
              NODE_ENV: 'production'
          }
      };
```


Step 1: How to use Redux with Polymer 3
Step-1 shows how to use polymer-redux.

Install dependencies
npm install --save polymer-redux@next

npm install --save redux

Create a folder named redux where we put in all our redux related files and create a file named redux-mixin.js

In the current step of this tutorial, we pack everything into one file.

importing the dependencies
```js
import {createStore} from '../../node_modules/redux/src/index.js'
import {createMixin} from '../../node_modules/polymer-redux/polymer-redux.js'
```

create a initialState (this could be empty).
const initialState = { 
    todos: [ 
        {text: ‘Buy Veggies’},
        {text: ‘Buy Fruits’},
        {text: ‘Buy Pizza’},
    ],
};

Next, we’ll define the reducer itself and return the initial state if our previous state is not defined.
```
const reducer = (state, action) => {
    if (!state) {
        return initialState;
    }
    const todos = state.todos.slice(0); //this will make a copy
    switch (action.type) {
        case 'ADD_TODO':
            todos.push({text:action.todo});
            break;
    }
    return Object.assign({}, state, {todos: todos});
};
```
this is how Object.assign works 
```
#########################
var o1 = { a: 1, b: 1, c: 1 };
var o2 = { b: 2, c: 2 };
var o3 = { c: 3 };

var obj = Object.assign({}, o1, o2, o3);
console.log(obj); // { a: 1, b: 2, c: 3 }
#########################

```
Now we only have to create the store as well as the mixin we will use in our Polymer elements.

// Create a Redux store
const store = createStore(reducer);
export const ReduxMixin = createMixin(store);

We created ReduxMixin which we can use in our elements to give them automagic redux.

now we will create a new element <todo-list>

```
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import {ReduxMixin} from '../redux/redux-mixin.js'

/**
 * @customElement
 * @polymer
 */
export class Todo extends ReduxMixin(PolymerElement) {
    static get is() {
        return 'todo-list';
    }
    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
        </style>
        <ul>
            <dom-repeat items="[[todos]]">
                <template>
                    <li>
                        <button on-click="_remove">[[item.text]]</button>
                    </li>
                </template>
            </dom-repeat>
        </ul>
    `;
    }

    static get properties() {
        return {
            todos: {
                type: Array,
                readOnly: true,
            },
        };
    }

    static mapStateToProps(state, element) {
        
    }
    static mapDispatchToEvents(dispatch, element) {
       
    }

    _remove(e) {
        
    }
}
window.customElements.define(Todo.is, Todo);
```

Now implement the mapStateToProps so that we have a link between the todos

then we will implement the _remove(e) so that this will dispatch event

then you can implement the mapDispatchToEvents so that we create a new event type REMOVE_TODO


now you go back to your redux-mixin.js and add a switch case for this logic and implement this.


now we will create a new element <todo-input>

```
import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {ReduxMixin} from '../redux/redux-mixin.js'
import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/paper-input/paper-input.js';


/**
 * @customElement
 * @polymer
 */
export class TodoInput extends ReduxMixin(PolymerElement) {
    static get is() {
        return 'todo-input';
    }

    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
            
            #input{
                min-width:400px;
            }
        </style>
        <paper-input id="input" label="Add todo (press enter)" on-keydown="_addOnEnter"></paper-input>

    `;
    }
}

window.customElements.define(TodoInput.is, TodoInput);
```

Now implement the _addOnEnter, mapDispatchToEvents so that we can add TODO's

then your done :)