const { Given,When,Then } = require('cucumber');

Given(/^Go to the route "([^"]*)"$/, function (url) {
    browser.url('http://localhost:8081' + url);
    return browser.isVisible("todo-redux-app");
});

When(/^I click the ([^"]*)$/, function (element) {
      browser.shadowDomElement(['todo-redux-app', 'create-to-do', element])
      .click();
  });
When(/^I wait (\d+) seconds$/, function (seconds) {
    browser.pause(seconds*1000);
});

When(/^I input ([^"]*)$/, function (input) {
    browser.shadowDomElement(['todo-redux-app', 'create-to-do', 'input'])
      .value=input;
});




  function findInShadowDom(selectors) {
    if (!Array.isArray(selectors)) {
      selectors = [selectors];
    }
  
    function findElement(selectors) {
      var currentElement = document;
      for (var i = 0; i < selectors.length; i++) {
        if (i > 0) {
          currentElement = currentElement.shadowRoot;
        }
  
        if (currentElement) {
          currentElement = currentElement.querySelector(selectors[i]);
        }
  
        if (!currentElement) {
          break;
        }
      }
  
      return currentElement;
    }
  
    if (!(document.body.createShadowRoot || document.body.attachShadow)) {
      selectors = [selectors.join(' ')];
    }
  
    return findElement(selectors);
  }
  
  /**
   * Add a command to return an element within a shadow dom.
   * The command takes an array of selectors. Each subsequent
   * array member is within the preceding element's shadow dom.
   *
   * Example:
   *
   *     const elem = browser.shadowDomElement(['foo-bar', 'bar-baz', 'baz-foo']);
   *
   * Browsers which do not have native ShadowDOM support assume each selector is a direct
   * descendant of the parent.
   */
  browser.addCommand("shadowDomElement", function(selector) {
    return this.execute(findInShadowDom, selector);
  });
  
  /**
   * Provides the equivalent functionality as the above shadowDomElement command, but
   * adds a timeout. Will wait until the selectors match an element or the timeout
   * expires.
   *
   * Example:
   *
   *     const elem = browser.waitForShadowDomElement(['foo-bar', 'bar-baz', 'baz-foo'], 2000);
   */
  browser.addCommand("waitForShadowDomElement", function async(selector, timeout, timeoutMsg, interval) {
    return this.waitUntil(() => {
      const elem = this.execute(findInShadowDom, selector);
      return elem && elem.value;
    }, timeout, timeoutMsg, interval)
      .then(() => this.execute(findInShadowDom, selector));
  });